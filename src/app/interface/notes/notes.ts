export interface Notes {
  _id?: string;
  title: string;
  create_time?: string;
  content?: string;
}
