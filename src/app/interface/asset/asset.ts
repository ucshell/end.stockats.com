import { PosStock } from '../stock/stock';

export interface Asset {
  init_asset: number;                             // 初始资金
  asset: number;                                  // 当前资金
  history_asset: Array<{ date: string, asset: number }>; // 历史资金数据
  position: Array<PosStock>;                      // 当前持仓股票
}
