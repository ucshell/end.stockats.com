export interface Stock {
  symbol: string;
  name?: string;
  cur_up: string | number;
  cur_price: number;
}

export const StockHeader: any = [
  { prop: 'symbol', title: '代码' },
  //  { prop: 'name', title: '名称' },
  { prop: 'cur_up', title: '当前涨幅' },
  { prop: 'cur_price', title: '当前价格' },
]

export interface RecStock extends Stock {
  rec_price: number;            // 推荐价格
  exp_price: number;            // 预期价格
  rec_date: string;             // 推荐日期
  exp_date: string;             // 期望日期
  exp_up: string;               // 预期上涨
}

export const RecStockHeader: any = StockHeader.concat([
  { prop: 'rec_price', title: '推荐价格' },
  { prop: 'exp_price', title: '预期价格' },
  { prop: 'rec_date', title: '推荐日期' },
  { prop: 'exp_date', title: '期望日期' },
  { prop: 'exp_up', title: '预期涨幅' }
]);


export interface PosStock extends RecStock {
  // 持仓推荐股票
  lot?: number;                 // 成交手数
  buy_price?: number;
  sell_price?: number;
  buy_date?: string;
  sell_date?: string;
  sell_up?: string;
}

export const PosStockHeader = RecStockHeader.concat([
  { prop: 'buy_price', title: '买入价格' },
  { prop: 'sell_price', title: '卖出价格' },
  { prop: 'buy_price', title: '买入价格' },
  { prop: 'buy_date', title: '买入日期' },
  { prop: 'sell_date', title: '卖出日期' },
  { prop: 'sell_up', title: '卖出获利' },
]);
