import express = require('express');
import session = require('express-session');

export interface Session extends Express.Session {
  login?: boolean,
  user?: {
    _id: string,			// 帐号数据库ID
    username: string,		// 帐号名
    admin: boolean		// 是否有管理员权限
  }
}
