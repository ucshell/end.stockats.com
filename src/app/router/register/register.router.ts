import { Router, Request, Response } from 'express';
import { RegisterHandler } from 'handler';

const router: Router = Router();
const handler: RegisterHandler = new RegisterHandler();

router.route('/')
  .post(handler.register)

export const registerRouter: Router = router;
