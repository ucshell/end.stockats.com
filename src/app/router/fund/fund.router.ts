import { Router, Request, Response } from 'express';
import { FundHandler } from 'handler';

const router: Router = Router();
const handler: FundHandler = new FundHandler();

router.route('/')
  .get(handler.list)
  .post(handler.create)

router.route('/:id')
  .get(handler.get)
  .put(handler.update)
  .delete(handler.remove)

export const fundRouter: Router = router;
