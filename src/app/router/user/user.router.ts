import { Router, Request, Response } from 'express';
import { UserHandler } from 'handler';

const router: Router = Router();
const handler: UserHandler = new UserHandler();

router.route('/')
  .get(handler.get)
  .post(handler.create)
  .delete(handler.remove)
  .put(handler.update)


router.route('/fund')
  .get(handler.listFund)
  .post(handler.createFund)

router.route('/fund/:id')
  .delete(handler.removeFund)

export const userRouter: Router = router;
