import { Router, Request, Response } from 'express';
import { UsersHandler } from 'handler';

const router: Router = Router();
const handler: UsersHandler = new UsersHandler();

router.route('/')
  .get(handler.list)
  .post(handler.create)

router.route('/:id')
  .get(handler.get)
  .delete(handler.remove)
  .put(handler.update)

export const usersRouter: Router = router;
