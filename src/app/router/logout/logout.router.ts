import { Router, Request, Response } from 'express';
import { LogoutHandler } from 'handler';

const router: Router = Router();
const handler: LogoutHandler = new LogoutHandler();

router.route('/')
  .get(handler.logout)

export const logoutRouter: Router = router;
