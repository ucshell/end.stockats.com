import { Router, Request, Response } from 'express';
import { NotesHandler } from 'handler';

const router: Router = Router();
const handler: NotesHandler = new NotesHandler();

router.route('/')
  .get(handler.list)
  .post(handler.create)

router.route('/:id')
  .get(handler.get)
  .put(handler.update)
  .delete(handler.remove)

export const notesRouter: Router = router;
