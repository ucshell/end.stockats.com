import { Router, Request, Response } from 'express';
import { LoginHandler } from 'handler';

const router: Router = Router();
const handler: LoginHandler = new LoginHandler();

router.route('/')
  .post(handler.login)

export const loginRouter: Router = router;
