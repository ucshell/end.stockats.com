import { Schema, model } from 'mongoose';

const NotesSchema = new Schema({
  title: String,
  create_time: {
    type: Date,
    default: Date.now
  },
  content: String
});

NotesSchema.methods = {};
NotesSchema.statics = {};

export const NotesModel = model('notes', NotesSchema);
