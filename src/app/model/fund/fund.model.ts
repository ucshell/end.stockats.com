import { Schema, model } from 'mongoose';

const FundSchema = new Schema({
  symbol: Number,
  name: String,
  type: Number,
  create_time: {
    type: Date,
    default: Date.now
  },
  rec_price: Number
});

FundSchema.methods = {};
FundSchema.statics = {};

export const FundModel = model('fund', FundSchema);


const ATSFundSchema = new Schema({
  name: String,
  desc: String,
  create_time: {
    type: Date,
    default: Date.now
  },
  position: [{
    type: Schema.Types.ObjectId,
    ref: 'fund'
  }],
  total_up: Number,
  exp_up: Number,
  cur_up: Number
});

ATSFundSchema.methods = {};
ATSFundSchema.statics = {};

export const ATSFundModel = model('atsfund', ATSFundSchema);
