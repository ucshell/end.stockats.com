import { Schema, model } from 'mongoose';

const UserSchema = new Schema({
  username: { type: String, unique: true },
  nickname: String,
  avatar: String,
  password: String,
  email: String,
  phone: String,
  admin: Boolean,
  fund: [{                      // 关注的基金
    type: Schema.Types.ObjectId,
    ref: 'atsfund'
  }]
});

UserSchema.methods = {};
UserSchema.statics = {};

export const UserModel = model('user', UserSchema);
