import { Request, Response } from 'express';
import { UserModel } from 'model';
import { Database } from 'class';

export class UserData extends Database {
  constructor() {
    super();
    this.model = UserModel;
  }
}
