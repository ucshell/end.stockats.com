import { Request, Response } from 'express';
import { FundModel, ATSFundModel } from 'model';
import { Database } from 'class';

export class FundData extends Database {
  constructor() {
    super();
    this.model = FundModel;
  }
}

export class ATSFundData extends Database {
  constructor() {
    super();
    this.model = ATSFundModel;
  }
}
