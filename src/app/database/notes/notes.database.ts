import { Request, Response } from 'express';
import { NotesModel } from 'model';
import { Database } from 'class';

export class NotesData extends Database {
  constructor() {
    super();
    this.model = NotesModel;
  }
}
