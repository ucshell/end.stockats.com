import * as mongoose from 'mongoose';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as httpLogger from 'morgan';
import { Request, Response, NextFunction } from 'express';
import * as session from 'express-session';
import * as cookie from 'cookie-parser';
import {
  DBService,
  SessionService
} from 'service';

import {
  notesRouter,
  userRouter,
  usersRouter,
  loginRouter,
  registerRouter,
  logoutRouter,
  fundRouter
} from 'router';

export class AppServer {
  app: express.Application;
  ip: string;
  port: string;
  uri: string;
  db: DBService;
  session: SessionService;

  constructor(ip: string, port: string, uri: string) {
    this.app = express();
    this.ip = ip;
    this.port = port;
    this.uri = uri;
  }

  initDB() {
    this.db = new DBService(this.uri);
    this.session = new SessionService(this.uri);
  }

  initLogger() {
    this.app.use(httpLogger('dev'));
  }

  initSession() {
    this.app.use(cookie());
    this.app.use(session({
      secret: 'stockats.com',
      cookie: { maxAge: 60 * 1000 * 30 },
      resave: false,
      saveUninitialized: false,
      store: this.session.memory()
    }));
  }

  initMiddleware() {
    this.app.use(bodyParser.json({ limit: '1mb' }));
    this.app.use(bodyParser.urlencoded({ extended: true }));
  }

  initRouter() {
    const routerMap: any = {
      '/notes': notesRouter,
      '/user': userRouter,
      '/login': loginRouter,
      '/register': registerRouter,
      '/logout': logoutRouter,
      '/fund': fundRouter,
      '/users': usersRouter
    }

    Object.keys(routerMap).forEach(key => {
      this.app.use(key, routerMap[key]);
    })

  }


  initServer() {
    this.initDB();
    this.initLogger();
    this.initMiddleware();
    this.initSession();
    this.initRouter();
  }

  startServer() {
    this.initServer();
    this.app.listen(this.port, () => {
      console.log(`Listen of http://localhost:${this.port}`);
    });
  }
}
