import { Request, Response } from 'express';
import { Database } from '../database/database';
import { Types } from 'mongoose';

export abstract class RouterHandler {
  data: Database;

  construct() { }

  initBind() {
    this.list = this.list.bind(this);
    this.get = this.get.bind(this);
    this.create = this.create.bind(this);
    this.update = this.update.bind(this);
    this.remove = this.remove.bind(this);
  }

  checkID(id: string) {
    return Types.ObjectId.isValid(id);
  }

  list(req: Request, res: Response) {
    this.data.list()
      .then((data: any) => {
        this.send(res, data)
      }).catch((err: any) => {
        this.sendCode(res, 500);
      })

  }

  get(req: Request, res: Response) {
    // TODO: 暂不支持多个id同时查询
    if (!this.checkID(req.params.id)) {
      this.sendCode(res, 500);
      return
    }
    this.data.get(req.params.id)
      .then((data: any) => {
        this.send(res, data);
      }).catch((err: any) => {
        this.sendCode(res, 500);
      })
  }

  create(req: Request, res: Response) {
    this.data.create(req.body)
      .then((data: any) => {
        this.send(res, data);
      })
      .catch((err: any) => {
        this.sendCode(res, 500);
      })
  }

  update(req: Request, res: Response) {
    if (!this.checkID(req.params.id)) {
      this.sendCode(res, 500);
      return
    }
    this.data.update(req.params.id, req.body)
      .then((data: any) => {
        this.send(res, data);
      }).catch((err: any) => {
        this.sendCode(res, 500);
      })
  }

  remove(req: Request, res: Response) {
    if (!this.checkID(req.params.id)) {
      this.sendCode(res, 500);
      return
    }

    this.data.remove(req.params.id)
      .then((data: any) => {
        this.send(res, { _id: req.params.id });
      })
      .catch((err: any) => {
        this.sendCode(res, 500);
      })
  }

  // TODO: will update flow function
  errorHandler(res: Request, err: any) {

  }

  send(res: Response, data: any) {
    res.send(JSON.stringify({ code: 0, data: data }));
  }

  sendCode(res: Response, code: number) {
    res.send(JSON.stringify({ code: code, data: [] }));
  }
}
