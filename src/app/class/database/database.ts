import {
  DocumentQuery,
  Document,
  Query,
  Types,
  Model
} from 'mongoose';

export abstract class Database {
  model: Model<Document>;

  constructor() { }

  list(): (DocumentQuery<Document[], Document>) {
    return this.model.find({});
  }

  get(id: string): (DocumentQuery<Document | null, Document>) {
    return this.model.findById(id);
  }

  create(data: any): any {
    return this.model.create(data);
    //    return (new this.model(data)).save();
  }

  update(id: string, data: any): (DocumentQuery<Document | null, Document>) {
    return this.model.update({ _id: id }, data);
  }

  remove(id: string): (Query<void>) {
    return this.model.remove({ _id: id })
  }

  find(data: any) {
    return this.model.find(data);
  }
}
