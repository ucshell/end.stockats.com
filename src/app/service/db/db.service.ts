import * as mongoose from 'mongoose';

export class DBService {

  constructor(uri: string) {
    (<any>mongoose).Promise = global.Promise
    mongoose.connect(uri, { useMongoClient: true });
  }

  static connection() {
    return mongoose.connection;
  }
}
