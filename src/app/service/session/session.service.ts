import * as mongoose from 'mongoose';
import * as session from 'express-session';
import * as mongoStore from 'connect-mongo';
import * as mongodb from 'mongodb';
import { MongoStoreFactory } from 'connect-mongo';

export class SessionService {
  session: MongoStoreFactory;

  constructor(private uri: string) {
    this.session = mongoStore(session);
  }

  memory() {
    return new this.session({
      url: this.uri,
      collection: 'session',
      ttl: 30 * 24 * 60 * 60
    })
  }
}
