import { Router, Request, Response } from 'express';
import { NotesData } from 'database';
import {
  Database,
  RouterHandler
} from 'class';
import { Notes } from 'interface';

export class NotesHandler extends RouterHandler {
  //  data: NotesData;

  constructor() {
    super();
    this.initBind();
    this.data = new NotesData();
  }

  create(req: Request, res: Response) {
    const notes: Notes = req.body;
    this.data.create(notes)
      .then((data: Notes) => {
        this.send(res, data);
      })
      .catch((err: any) => {
        this.sendCode(res, 500);
      })
  }


  update(req: Request, res: Response) {
    this.data.update(req.params.id, req.body)
      .then((data: any) => {
        this.send(res, data);
      })
      .catch((err: any) => {
        this.sendCode(res, 500);
      })
  }
}
