import { Router, Request, Response } from 'express';
import { UserData } from 'database';
import {
  Database,
  RouterHandler
} from 'class';
import { Session } from 'interface';

export class LoginHandler extends RouterHandler {
  //  data: UserData;

  constructor() {
    super();
    this.initBind();
    this.login = this.login.bind(this);
    this.data = new UserData();
  }

  login(req: Request, res: Response) {
    const session: Session = <Session>req.session;
    this.data.find({ username: req.body.username })
      .then((data: any) => {
        if (!data.length) {
          this.sendCode(res, 205);
          return;
        }
        data = data[0];
        if (data.password !== req.body.password) {
          this.sendCode(res, 205);
          return;
        }

        session.login = true
        session.user = {
          _id: data._id,
          username: data.username,
          admin: data.admin
        }
        this.send(res, data);
      })
  }
}
