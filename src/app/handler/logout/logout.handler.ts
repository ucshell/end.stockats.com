import { Router, Request, Response } from 'express';
import { UserData } from 'database';
import {
  Database,
  RouterHandler
} from 'class';
import { Session } from 'interface';

export class LogoutHandler extends RouterHandler {
  //  data: UserData;

  constructor() {
    super();
    this.initBind();
    this.logout = this.logout.bind(this);
  }

  logout(req: Request, res: Response) {
    const session: Session = <Session>req.session;
    if (session.login) {
      session.login = false;
    }
    this.sendCode(res, 0);
  }
}
