import { Router, Request, Response } from 'express';
import { UserData } from 'database';
import {
  Database,
  RouterHandler
} from 'class';
import { Session } from 'interface';

export class UserHandler extends RouterHandler {
  //  data: UserData;

  constructor() {
    super();
    this.initBind();
    this.data = new UserData();
    this.createFund = this.createFund.bind(this);
    this.removeFund = this.removeFund.bind(this);
    this.listFund = this.listFund.bind(this);
  }

  get(req: Request, res: Response) {
    const session: Session = <Session>req.session;
    if (!session.login || !session.user) {
      this.sendCode(res, 401);
      return;
    }
    this.send(res, session.user);
  }

  create(req: Request, res: Response) {
    const session: Session = <Session>req.session;
    this.data.find({ username: req.body.username })
      .then((data: any) => {
        if (!data.length) {
          this.sendCode(res, 205);
          return;
        }
        data = data[0];
        if (data.password !== req.body.password) {
          this.sendCode(res, 205);
          return;
        }

        session.login = true
        session.user = data;
        this.send(res, data);
        // this.data.get(data._id).populate('fund').exec((err, docs) => {
        //   this.send(res, docs);
        // })
      }).catch((err: any) => {
        this.sendCode(res, 500);
      })
  }

  remove(req: Request, res: Response) {
    const session: Session = <Session>req.session;
    if (session.login) {
      session.login = false;
      delete session.user;
    }
    this.sendCode(res, 0);
  }


  listFund(req: Request, res: Response) {
    const session: Session = <Session>req.session;
    let user = session.user;
    if (!user) {
      this.sendCode(res, 401);
      return;
    }

    this.data.get(user._id)
      .populate('fund').exec((err, docs: any) => {
        this.send(res, docs.fund);
      })
  }

  // 增加基金关注
  createFund(req: Request, res: Response) {
    const session: Session = <Session>req.session;
    let user = session.user;
    if (!user) {
      this.sendCode(res, 401);
      return;
    }

    this.data.get(user._id)
      .then((user: any) => {
        user.fund.push(req.body.fund)
        this.data.update(user._id, user).then(data => {
          this.send(res, {})
        })
      }).catch(err => {
        this.sendCode(res, 500);
      })
  }

  removeFund(req: Request, res: Response) {
    const session: Session = <Session>req.session;
    let user = session.user;
    if (!user) {
      this.sendCode(res, 401);
      return;
    }

    this.data.get(user._id)
      .then((user: any) => {
        user.fund = user.fund.filter((id: any) => id != req.params.id)
        this.data.update(user._id, user).then(data => {
          this.send(res, { _id: req.params.id });
        })
      }).catch(err => {
        this.sendCode(res, 500);
      })
  }
}
