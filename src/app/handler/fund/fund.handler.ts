import { Router, Request, Response } from 'express';
import { FundData, ATSFundData } from 'database';
import {
  Database,
  RouterHandler
} from 'class';

export class FundHandler extends RouterHandler {
  //  data: FundData;
  fundData: any;
  constructor() {
    super();
    this.initBind();
    this.data = new ATSFundData();
    this.fundData = new FundData();
  }

  create(req: Request, res: Response) {
    if (!req.body.position || !req.body.position.length) {
      this.data.create(req.body)
        .then((data: any) => {
          this.send(res, data);
        })
        .catch((err: any) => {
          this.sendCode(res, 500);
        })
      return;
    }

    this.fundData.create(req.body.position)
      .then((data: any) => {
        req.body.position = data.map((elem: any) => elem._id);
        this.data.create(req.body)
          .then((data: any) => {
            this.data.get(data._id)
              .populate('position').exec((err, docs) => {
                this.send(res, docs);
              })
          })
          .catch((err: any) => {
            this.sendCode(res, 500);
          })
      })
  }

  update(req: Request, res: Response) {
    if (!this.checkID(req.params.id)) {
      this.sendCode(res, 500);
      return
    }
    req.body = {
      position: [
        { symbol: 11001, name: 'Y11001 Name', type: 1, rec_price: 1.27 },
        { symbol: 11002, name: 'Y11002 Name', type: 1, rec_price: 2.37 }
      ]
    }

    const newPosition = [];
    const updatePosition = [];

    // req.body.position.forEach(elem => if ())
    this.data.update(req.params.id, req.body)
      .then((data: any) => {
        this.send(res, data);
      }).catch((err: any) => {
        this.sendCode(res, 500);
      })
  }

}
