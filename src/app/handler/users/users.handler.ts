import { Router, Request, Response } from 'express';
import { UserData } from 'database';
import {
  Database,
  RouterHandler
} from 'class';
import { Session } from 'interface';

export class UsersHandler extends RouterHandler {
  //  data: UserData;

  constructor() {
    super();
    this.initBind();
    this.data = new UserData();
  }

}
