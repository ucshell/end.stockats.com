import { Router, Request, Response } from 'express';
import { UserData } from 'database';
import {
  Database,
  RouterHandler
} from 'class';

export class RegisterHandler extends RouterHandler {
  //  data: UserData;

  constructor() {
    super();
    this.initBind();
    this.register = this.register.bind(this);
    this.data = new UserData();
  }

  register(req: Request, res: Response) {
    this.data.find({ username: req.body.username })
      .then((data: any) => {
        if (data.length) {
          this.sendCode(res, 202);
          // 已经存在
          return;
        }
        this.data.create(req.body).then((data: any) => {
          this.sendCode(res, 0);
        })
      })
  }
}
