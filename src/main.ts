import { AppServer } from './app/app.module';

const server = new AppServer('127.0.0.1', '8888', 'mongodb://localhost/stockats');
server.startServer();
