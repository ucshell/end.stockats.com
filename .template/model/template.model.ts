import { Schema, model } from 'mongoose';

const TemplateSchema = new Schema({
});

TemplateSchema.methods = {};
TemplateSchema.statics = {};

export const TemplateModel = model('template', TemplateSchema);
