import { Router, Request, Response } from 'express';
import { TemplateData } from 'database';
import {
  Database,
  RouterHandler
} from 'class';

export class TemplateHandler extends RouterHandler {
  //  data: TemplateData;

  constructor() {
    super();
    this.initBind();
    this.data = new TemplateData();
  }

  create(req: Request, res: Response) {
    this.data.create({})
      .then((data: any) => {
        this.send(res, data);
      })
      .catch((err: any) => {
        this.sendError(res, 500);
      })
  }

  update(req: Request, res: Response) {
    if (!this.checkID(req.params.id)) {
      this.sendError(res, 500);
      return
    }
    this.data.update(req.params.id, req.body)
      .then((data: any) => {
        this.send(res, data);
      }).catch((err: any) => {
        this.sendError(res, 500);
      })
  }

}
