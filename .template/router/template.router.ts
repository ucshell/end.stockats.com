import { Router, Request, Response } from 'express';
import { TemplateHandler } from 'handler';

const router: Router = Router();
const handler: TemplateHandler = new TemplateHandler();

router.route('/')
  .get(handler.list)
  .post(handler.create)

router.route('/:id')
  .get(handler.get)
  .put(handler.update)
  .delete(handler.remove)

export const templateRouter: Router = router;
